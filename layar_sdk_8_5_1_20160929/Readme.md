# Layar iOS SDK 8.5.1


## Hardware and Software Requirements


    - iPhone 4S or higher
    - iPad 2 or higher
    - iOS version 8.0 or higher
    - Xcode 7 or higher
    - iPhone SDK 9.2 or higher
    - A published layer or campaign in your Layar account. For getting started please see http://devsupport.layar.com/forums/20604191-getting-started or http://devsupport.layar.com/forums/20603886-getting-started

## License  


    By embedding the Layar SDK framework in your application, you agree to the Terms and Conditions described in the Layar_SDK_License_Agreement_v5_4_20131016.pdf file which can be found in the Layar SDK package or as Attachment 4 to our Layar Terms and Conditions for Developers and Publishers at https://www.layar.com/legal/terms-developers/


## Package Contents


###### LayarSDK.framework
        The iOS Static Framework that you can embed into your app.

###### LayarSDK.framework/Resources/LayarSDKResources.bundle
        The bundle with resources that you may change to your convenience.

###### LayarSDK.podspec
        The CocoaPods specification for LayarSDK. Please see Installation - Cocoapods for more details.

###### External-Libraries
        Includes external libraries used by the LayarSDK.

###### Layar SDK Documentation/html/index.html
        Please start here for the documentation

###### Layar SDK Documentation/html/com.layar.layarSDK.docset
        The same documentation as a docset that you can add to XCode.

###### Layar SDK Documentation/InstallDocSet.sh
        Shell script to install docset to XCode

###### Layar_SDK_License_Agreement_v5_4_20131016.pdf
        The license agreement for the Layar SDK

###### Layar SDK app submission guidelines_20131023_DG.pdf
        A document giving some guidelines to help you through the Apple app review process

###### This readme.txt file


## Installation


### Cocoapods Installation (Recommended)

    Unzip the contents of the LayarSDK Zip Folder that you have downloaded into a folder preferably inside your repository.

    In your podfile specify add a pod called LayarSDK and specify the path to the saved LayarSDK Folder on your local filesystem where you 
    unzipped the LayarSDK. 

    Run 'pod install' on your command line to install LayarSDK.

```ruby
pod 'LayarSDK', :path => '~/Documents/LayarSDK'
```

### Manual Installation

    Unzip the contents of the LayarSDK Zip Folder that you have downloaded 
    into a folder preferably inside your repository.  

##### Add the LayarSDK Framework to your project
    In your xcode project right click on a group or the project and click "Add Files to <Project Name>". Now navigate to the LayarSDK folder that you unzipped and select the LayarSDK Framework.

##### Add the LayarSDKResources.bundle to your project
    In your xcode project right click on a group or the project and click "Add Files to <Project Name>". Now navigate to the LayarSDK folder that you unzipped and select the LayarSDKResources.Bundle inside the LayarSDK Framework folder.

##### Add External Libraries
    In your xcode project right click on a group or the project and click "Add Files to <Project Name>". Now navigate to the LayarSDK folder that you unzipped and select all the libraries in the External-Libraries Folder. At the time of writing this documentation the external libraries are libCocoaLumberjack.a, libREComposeViewController.a and libMPOAuth.a. Add all of them to your project.

##### Add Required iOS Libraries
    Now click your app target and select the 'Build Phases' pane. In the build phases pane expand the "Link Binary With Libraries Section". Click the '+' button and add the following dynamic libraries: 
    
    'libstdc++.tbd'
    'libz.tbd'
    'libiconv.tbd'
    'libxml2.tbd'

##### Add Required iOS Frameworks
    Click the '+' button again the "Link Binary With Libraries Section" and add the following dynamic frameworks:
    
    'AVFoundation'
    'CoreMedia'
    'AddressBook'
    'AddressBookUI'
    'CoreData'
    'Social'
    'CoreMotion'
    'MapKit'
    'MessageUI'
    'CoreLocation'
    'CoreTelephony'
    'WebKit'


## Usage

##### Initialize a LayarSDK Object with your Layar SDK Consumer Key and Consumer Secret

     In your app (preferable in your app delegate) add #import <LayarSDK/LayarSDK.h> to the top of the file to import the LayarSDK. Then create a LayarSDK Object with your consumer key and consumer secret.

```objective-c
#import <LayarSDK/LayarSDK.h>
...
...
NSString *consumerKey = @"yourkey"; 
NSString *consumerSecret = @"yoursecret";
LayarSDK* layarSDK = [LayarSDK layarSDKWithConsumerKey:consumerKey 
andConsumerSecret:consumerSecret andDelegate:nil];
...
...
```

##### Create and show a scan view. 

     To show a scan view controller use the viewControllerForScanningWithCompletion: method of the LayarSDK Object 
     to create a LayarSDK View Controller. 

     Then present it using the presentViewController:animated:completion: method in UIViewController. 

     Here we also add a close button to the view controller which we connect to 
     our dismissScanViewController so that we can dismiss our scan view.

```objective-c
#import <LayarSDK/LayarSDK.h>
...
...
[layarSDK viewControllerForScanningWithCompletion:^(UIViewController<LayarSDKViewController>* viewController)
 {
    UIButton* closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 40.0f, 80.0f, 30.0f)];
    [closeButton setTitle:@"Close" forState:UIControlStateNormal];
         
    [closeButton addTarget:self action:@selector(dismissScanViewController) forControlEvents:UIControlEventTouchUpInside];
    [viewController.view addSubview:closeButton];
         
    [self presentViewController:viewController animated:YES completion:nil];
 }];
...
...
- (void)dismissScanViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
...
...
```

##### Get Delegate Callbacks

    It is also possible to get delegate callbacks from the LayarSDK so that we can do appropriate things in case an event happens. 

    To do this we give the LayarSDK a delegate parameter when we create it. We also declare that the object we set as delegate follows the LayarSDKDelegate protocol. Once we do this we can listen for events and do appropriate things when they happen. For example we show here how we can show an alert when a layar is launched.

```objective-c
#import <LayarSDK/LayarSDK.h>
@interface AppDelegate () <LayarSDKDelegate>
...
...
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    NSString *consumerKey = @"yourkey";
    NSString *consumerSecret = @"yoursecret";
    LayarSDK* layarSDK = [LayarSDK layarSDKWithConsumerKey:consumerKey
                                         andConsumerSecret:consumerSecret andDelegate:self];
    [layarSDK viewControllerForScanningWithCompletion:^(UIViewController<LayarSDKViewController>* viewController)
     {
         UIButton* closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 40.0f, 80.0f, 30.0f)];
         [closeButton setTitle:@"Close" forState:UIControlStateNormal];
         
         [closeButton addTarget:self action:@selector(dismissScanViewController) forControlEvents:UIControlEventTouchUpInside];
         [viewController.view addSubview:closeButton];
         
         [self.window.rootViewController presentViewController:viewController animated:YES completion:nil];
     }];

    return YES;
}

#pragma mark - LayarSDKDelegate

- (void)layarSDK:(LayarSDK*)layarSdk didLaunchLayer:(NSString*)layerName withTitle:(NSString*)layerTitle recognizedReferenceImageName:(NSString*)recognizedReferenceImageName onViewController:(UIViewController<LayarSDKViewController>*)viewController
{
    [[[UIAlertView alloc] initWithTitle:@"Layar" message:layerTitle delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

@end
```


## Documentation


    The documentation can be found in the documentation folder. 

    To install the docset into your xcode run the script InstallDocset.sh. 

    To view the documentaion in your browser just go the html folder and open _layar_s_d_k_8h.html in your browser. 

[Click here](Documentation/html/_layar_s_d_k_8h.html) to view it. 


## Changelog

##### September 28th 2016 - SDK 8.5.1
    Some updates to work with Xcode8

##### February 25th 2016 - SDK 8.5
    Updates for Layar API 8.5

##### February 23rd 2016 - SDK 8.4.4
    Added support for Custom Location Providers
    Added callback  (willSendPOIRequestWithParameters:(NSDictionary*)parameters) to customize parameters in a poi request
    Added method  (openURL:onExistingViewController:completion:) to not have to create a new view controller to open a new URL. 
    Fixed crashes with Action Sheets and BlocksKit by migrating to the new API of UIAlertControllers (8.0+)
    Added warnings to the Log that shows how many ARViewControllers are created and are alive at the given moment.
    To support custom location providers and many location updates a second, we now update POIs at least after one second after the last poi update.
    Added logic for not showing the compass calibration screen too often

##### September 23rd 2015 - SDK 8.4.3
    Fixed memory issue
    Updated SDK For iOS9 Support

##### May 18th 2015 - SDK 8.4.2
    Fixed another crash that happened while deallocating LayarSDK
    Added Custom Exposure for better tracking
    Made webview smoother and fixed a bug that was making them hard to touch on landscape
    Supporting Installation through cocoapods

##### April 13th 2015 - SDK 8.4.1
    Fixed a crash that happened on upgrade from older versions

##### March 3rd 2015 - SDK 8.4
    Fully customisable UI for your scan view
    Added support to change the view-angle of the pop-out view (2d or 3d view)
    Added support to hide the trigger image in the pop-out view
    Added support to automatically re-attach the content back to the trigger image after it was opened in the pop-out view
    Added support to tak and share a screenshot
    Added support to loop AR videos (only available using the Layar API)
    Added support to hide the audio player (only available using the Layar API)
    Added support to hide the loading indicators (only available using the Layar API)
    Added support to hide the replay button for AR videos (only available using the Layar API)

##### October 21st 2014 - SDK 8.3.2
    Fixed location support in geo layers for iOS 8 and above 

##### July 29th 2014 - SDK 8.3.1
    Fixed ARM64 support.

##### June 24th 2014 - SDK 8.3
    Added new capabilities for 3D models to support POI anchors, texture override, support for video and animated GIF textures
    updated our pop-out view so users can double tap or pinch to zoom, drag to move the 3D model around and rotate with two fingers.
    various bug fixes and improvements.

##### May 13th 2014 - SDK 8.2
    Fix for transparent animated GIF
    Fix for reporting stats for HTML-panels
    Fix for buttons not shown in front or back in regards to other buttons
    Removed dependency on SBJson library
    Added support for ARM64
    Various other bug fixes and improvements

##### March 11th 2014 - SDK 8.1
    The Layar SDK has been supplied with pop-out and additional APIs to alter the behaviour for this feature
    Various bug fixes and improvements

##### October 24th 2013 - SDK 8.0
    Support for Geo layers is back. Using Vision layers or Geo layers inside the SDK is subject to you having purchased the appropriate key to unlock that class of content.
    A lot of new delegate methods added allowing easier customization.
    Multiple bug-fixes and performance improvements.

##### June 24th 2013 - SDK 7.2.4
    Layar SDK has been supplied with delegate methods to receive callbacks for successfully scanned reference images and campaigns. A bug has been fixed that caused all statistics to be send twice.

##### April 11th 2013 - SDK 7.2.3
    Layar SDK now supports 5 additional languages: Dutch, German, Spanish, French and Japanese.

##### March 15th 2013 - SDK 7.2.0
    Layar SDK now supports QR-code scanning. Multiple bug-fixes and performance improvements.

##### March 6th 2013 - SDK 7.1.2
    Fixed crash to improve stability.

##### February 14th 2013 - SDK 7.1.1
    Fixed crash to improve stability.

##### January 15th 2013 - SDK 7.1.0
    Release of the LayarSDK.


## Contact and Information:


###### [Layar](http://www.layar.com): http://www.layar.com
###### [Layar SDK support](http://devsupport.layar.com/categories/6169-layar-sdk):  http://devsupport.layar.com/categories/6169-layar-sdk

